module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    paths: {
        src: 'lib',
        public: 'public'
    },
    jshint: {
        files: ['Gruntfile.js', '<%= paths.src %>/js/**/*.js'],
    },
    uglify: {
        app: {
            files: {
                '<%= paths.public %>/js/gallery.js': ['<%= paths.src %>/js/gallery.js']
            }
        }
    },
    watch: {
        files: ['<%= paths.src %>/**/*'],
        tasks: ['jshint', 'uglify']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['jshint', 'uglify']);

  grunt.registerTask('dev', ['watch']);

};
