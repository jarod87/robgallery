var express = require('express'),
	app = express(),
	path = require('path');

app.use(express.static(__dirname + '/public'));

// viewed at http://localhost:8080
app.get('/', function(req, res) {
	console.log(process.env.NODE_ENV);
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/img/:id', function(req, res) {
	res.send(req.params);
});

app.listen(3000);
